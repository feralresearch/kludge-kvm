# !/bin/bash
# /usr/local/bin/watch -n3 /Users/andrew/Code/Projects/kludge-kvm &>/dev/null &
#

FILE=/Users/andrew/Code/Projects/kludge-kvm/.present
if /usr/sbin/ioreg -p IOUSB | /usr/bin/grep -q Ducky; then
    if test ! -f "$FILE"; then
        /usr/local/bin/ddcctl -d 1 -i 27
        /usr/bin/touch $FILE
    fi
else
    if test -f "$FILE"; then
        /usr/local/bin/ddcctl -d 1 -i 15
        /bin/rm $FILE
    fi
fi